﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetAll()
        {
            return Ok(_parkingService.GetVehicles());
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<Vehicle> GetById([FromRoute]string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"))
            {
                return BadRequest(
                    "Id must have a format according to pattern XX-YYYY-XX\nwhere 'X'- char in upper case, 'Y' - a number from 0 to 9");
            }

            try
            {
                return Ok(_parkingService.GetVehicles().First(vehicle => vehicle.Id.Equals(id)));
            }
            catch (InvalidOperationException)
            {
                return NotFound($"Didn't found vehicle with Id='{id}'.");
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]VehicleViewModel model)
        {
            try
            {
                var vehicle = new Vehicle(model.Id, (VehicleType) model.VehicleType, (decimal) model.Balance);
                _parkingService.AddVehicle(vehicle);
                return CreatedAtAction(nameof(GetById), new {id = vehicle.Id}, vehicle);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidOperationException)
            {
                return BadRequest("Parking is full for now.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("{id}")]
        //[Route("{id:regex(^[[A-Z]]{{2}}-[[0-9]]{{4}}-[[A-Z]]{{2}}$)}")]
        public IActionResult DeleteById([FromRoute] string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"))
            {
                return BadRequest(
                    "Id must have a format according to pattern XX-YYYY-XX\nwhere 'X'- char in upper case, 'Y' - a number from 0 to 9");
            }

            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}