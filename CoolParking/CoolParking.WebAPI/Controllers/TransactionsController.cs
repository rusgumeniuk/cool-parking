﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;

using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [Route("last")]
        public ActionResult<IEnumerable<TransactionInfo>> Last()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<string> All()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle([FromBody]TopUpViewModel model)
        {
            try
            {
                _parkingService.TopUpVehicle(model.Id, model.Sum);
                return Ok(_parkingService.GetVehicles().First(vehicle => vehicle.Id.Equals(model.Id)));
            }
            catch (ArgumentException ex)
            {
                return ex.Message.Contains("id")
                    ? NotFound(ex.Message)
                    : (ActionResult<Vehicle>) BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}