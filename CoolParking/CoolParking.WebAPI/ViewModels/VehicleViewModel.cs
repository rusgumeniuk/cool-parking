﻿using CoolParking.WebAPI.Attributes;

using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.ViewModels
{
    public class VehicleViewModel
    {
        [VehicleRegistrationPlate(
            ErrorMessage = "Id must have a format XX-YYYY-XX, where 'X' - a char in upper case, 'Y' - a number from 0 to 9")]
        public string Id { get; set; }

        [Range(1, 4)]//min/max?
        public int VehicleType { get; set; }

        [PositiveDecimalNumber(ErrorMessage = "Balance of a new vehicle has to be more than 0.")]
        public decimal Balance { get; set; }
    }
}
