﻿using CoolParking.WebAPI.Attributes;

namespace CoolParking.WebAPI.ViewModels
{
    public class TopUpViewModel
    {
        [VehicleRegistrationPlate(
            ErrorMessage = "Id must have a format XX-YYYY-XX, where 'X' - a char in upper case, 'Y' - a number from 0 to 9")]
        public string Id { get; set; }

        [PositiveDecimalNumber(ErrorMessage = "Sum has to be more than 0")]
        public decimal Sum { get; set; }
    }
}
