﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class VehicleRegistrationPlateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var input = value?.ToString();
            return !string.IsNullOrWhiteSpace(input) && Regex.IsMatch(input, "^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
        }
    }
}
