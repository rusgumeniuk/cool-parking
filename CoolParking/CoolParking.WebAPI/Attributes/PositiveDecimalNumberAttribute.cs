﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class PositiveDecimalNumberAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var number = (decimal)value;
            return number > decimal.Zero;
        }
    }
}
