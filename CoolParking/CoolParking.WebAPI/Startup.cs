using System.IO;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        private static readonly string LogFilePath = $@"{Directory.GetCurrentDirectory()}\Transactions.log";

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();

            var withdrawtimer = new TimerService();
            var logTimer = new TimerService();
            var logger = new LogService(LogFilePath);

            services.AddSingleton<IParkingService>(_ => new ParkingService(withdrawtimer, logTimer, logger));

            withdrawtimer.Start();
            logTimer.Start();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
