﻿namespace CoolParking.ConsoleApp.Interfaces
{
    public interface IConsole
    {
        void DisplayMainMenu();
        void DisplayMenu(string title, string content);
        void DisplayBackToMainMenu();
    }
}
