﻿using CoolParking.BL.Interfaces;
using CoolParking.ConsoleApp.Models;

using System.Collections.Generic;

namespace CoolParking.ConsoleApp.Interfaces
{
    interface IParkingConsoleService : IParkingService
    {
        IList<VehicleDto> GetVehicleList();
    }
}
