﻿namespace CoolParking.ConsoleApp.Interfaces
{
    public interface IReader
    {
        decimal ParseDecimalNumber(string @string = null);
        decimal ParseDecimalNumber(decimal min, string @string = null);
        decimal ParseDecimalNumber(decimal min, decimal max, string @string = null);

        int ParseIntNumber(string @string = null);
        int ParseIntNumber(int min, string @string = null);
        int ParseIntNumber(int min, int max, string @string = null);

        string GetNotEmptyString(string @string = null);
    }
}
