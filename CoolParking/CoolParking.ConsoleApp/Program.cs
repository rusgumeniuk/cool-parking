﻿using CoolParking.ConsoleApp.Interfaces;
using CoolParking.ConsoleApp.Models;
using CoolParking.ConsoleApp.Services;

using System.Net.Http;

namespace CoolParking.ConsoleApp
{
    class Program
    {
        private static IParkingConsoleService _parkingService;

        private static IConsole _parkingConsole;

        static void Main(string[] args)
        {
            _parkingService = new ParkingHttpService(new HttpClient());

            //_parkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Bus, 20));
            //_parkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.PassengerCar, 100));
            //_parkingService.AddVehicle(new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Motorcycle, 30));

            _parkingConsole = ParkingConsole.GetInstance(new ConsoleReader(), _parkingService);


            while (true)
            {
                _parkingConsole.DisplayMainMenu();
            }
        }
    }
}
