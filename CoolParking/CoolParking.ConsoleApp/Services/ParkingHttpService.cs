﻿using CoolParking.BL.Models;
using CoolParking.ConsoleApp.Interfaces;
using CoolParking.ConsoleApp.Models;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CoolParking.ConsoleApp.Services
{
    class ParkingHttpService : IParkingConsoleService
    {
        private const string TransactionsUrl = "https://localhost:44330/api/Transactions";
        private const string VehiclesUrl = "https://localhost:44330/api/Vehicles";
        private const string ParkingUrl = "https://localhost:44330/api/Parking";

        private readonly HttpClient _httpClient;

        public ParkingHttpService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public decimal GetBalance()
        {
            var uri = $"{ParkingUrl}/balance";

            var response = SendRequestAsync(HttpMethod.Get, uri, null).Result;
            return GetResponseResultOrDefaultAsync<decimal>(response).Result;
        }

        public int GetCapacity()
        {
            var uri = $"{ParkingUrl}/capacity";

            var response = SendRequestAsync(HttpMethod.Get, uri, null).Result;
            return GetResponseResultOrDefaultAsync<int>(response).Result;
        }

        public int GetFreePlaces()
        {
            var uri = $"{ParkingUrl}/freePlaces";

            var response = SendRequestAsync(HttpMethod.Get, uri, null).Result;
            return GetResponseResultOrDefaultAsync<int>(response).Result;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var response = SendRequestAsync(HttpMethod.Get, VehiclesUrl, null).Result;
            return GetResponseResultOrDefaultAsync<ReadOnlyCollection<Vehicle>>(response).Result;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            var response = SendRequestAsync(HttpMethod.Post, VehiclesUrl, vehicle).Result;
            var result = GetResponseResultAsync(response).Result;
            ThrowExceptionIfNotSuccess(response, result);
        }



        public void RemoveVehicle(string vehicleId)
        {
            var uri = $"{VehiclesUrl}/{vehicleId}";

            var response = SendRequestAsync(HttpMethod.Delete, uri, null).Result;
            var result = GetResponseResultAsync(response).Result;
            ThrowExceptionIfNotSuccess(response, result);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var uri = $"{TransactionsUrl}/TopUpVehicle";
            var content = new { Id = vehicleId, Sum = sum };
            var response = SendRequestAsync(HttpMethod.Put, uri, content).Result;
            var result = GetResponseResultAsync(response).Result;
            ThrowExceptionIfNotSuccess(response, result);
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            var uri = $"{TransactionsUrl}/Last";

            var response = SendRequestAsync(HttpMethod.Get, uri, null).Result;
            var res = GetResponseResultOrDefaultAsync<TransactionInfo[]>(response).Result;
            return res;
        }

        public string ReadFromLog()
        {
            var uri = $"{TransactionsUrl}/All";

            var response = SendRequestAsync(HttpMethod.Get, uri, null).Result;
            var result = GetResponseResultAsync(response).Result;
            ThrowExceptionIfNotSuccess(response, result);
            return result;
        }

        public IList<VehicleDto> GetVehicleList()
        {
            var response = SendRequestAsync(HttpMethod.Get, VehiclesUrl, null).Result;
            return GetResponseResultOrDefaultAsync<IList<VehicleDto>>(response).Result;
        }


        public void Dispose()
        {

        }

        private static void ThrowExceptionIfNotSuccess(HttpResponseMessage response, string message)
        {
            if (!response.IsSuccessStatusCode)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.BadRequest:
                        {
                            try
                            {
                                var errors = JsonConvert.DeserializeXNode(message, "response");
                                if (errors == null)
                                    throw new Exception(message);
                                else
                                {

                                    var stringBuilder = new StringBuilder();
                                    foreach (var error in errors.Root.Element("errors").Elements())
                                    {
                                        stringBuilder.Append(error.Name).Append(" : ").AppendLine(error.Value);
                                    }

                                    throw new Exception(stringBuilder.ToString());
                                }
                            }
                            catch (Exception)
                            {
                                throw new Exception(message);
                            }
                        }
                    default:
                        throw new Exception(message);
                }
            }
        }

        private async Task<T> GetResponseResultOrDefaultAsync<T>(HttpResponseMessage responseMessage)
        {
            var result = await GetResponseResultAsync(responseMessage);
            return responseMessage.IsSuccessStatusCode && !string.IsNullOrWhiteSpace(result) ? JsonConvert.DeserializeObject<T>(result) : default;
        }

        private async Task<string> GetResponseResultAsync(HttpResponseMessage response)
        {
            return await response.Content.ReadAsStringAsync();
        }

        private async Task<HttpResponseMessage> SendRequestAsync(HttpMethod httpMethod, string url, object contentToSerialize)
        {
            var jsonObject = await JsonConvert.SerializeObjectAsync(contentToSerialize);
            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");

            var message = new HttpRequestMessage(httpMethod, url)
            {
                Content = content
            };

            return await _httpClient.SendAsync(message);
        }
    }
}
