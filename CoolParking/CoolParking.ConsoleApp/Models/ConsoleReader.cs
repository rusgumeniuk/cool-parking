﻿using CoolParking.ConsoleApp.Interfaces;

using System;

namespace CoolParking.ConsoleApp.Models
{
    internal class ConsoleReader : IReader
    {
        public decimal ParseDecimalNumber(string @string = null)
        {
            var input = @string ?? GetNotEmptyString();
            return decimal.TryParse(input, out var number) ? number : throw new FormatException($"We can't parse '{input}' to a decimal."); ;
        }
        public decimal ParseDecimalNumber(decimal min, string @string = null)
        {
            var number = ParseDecimalNumber(@string);
            return number >= min ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number more or equal {min}!"); ;
        }
        public decimal ParseDecimalNumber(decimal min, decimal max, string @string = null)
        {
            var number = ParseDecimalNumber(min, @string);
            return number <= max ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number from the range [{min} - {max}].");
        }


        public int ParseIntNumber(string @string = null)
        {
            var input = @string ?? GetNotEmptyString();
            return int.TryParse(input, out var number) ? number : throw new FormatException($"We can't parse '{input}' to an integer.");
        }
        public int ParseIntNumber(int min, string @string = null)
        {
            var number = ParseIntNumber(@string);
            return number >= min ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number more or equal {min}!"); ;
        }
        public int ParseIntNumber(int min, int max, string @string = null)
        {
            var number = ParseIntNumber(min, @string);
            return number <= max ? number : throw new ArgumentOutOfRangeException(null, $"Please, input the number from the range [{min} - {max}].");
        }

        public string GetNotEmptyString(string @string = null)
        {
            var input = (@string ?? Console.ReadLine())?.Trim();
            if (!string.IsNullOrEmpty(input)) return input;
            throw new ArgumentNullException(null, "Please, input something.");
        }
    }
}
