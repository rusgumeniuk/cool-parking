﻿using CoolParking.BL.Models;

namespace CoolParking.ConsoleApp.Models
{
    class VehicleDto
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
        public override string ToString()
        {
            return $"Id: {Id} | Type: {VehicleType} | Current balance: {Balance}";
        }
    }
}
