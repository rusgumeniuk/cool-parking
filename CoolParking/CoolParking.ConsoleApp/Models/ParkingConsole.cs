﻿using CoolParking.BL.Models;
using CoolParking.ConsoleApp.Interfaces;

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.ConsoleApp.Models
{
    internal class ParkingConsole : IConsole
    {
        private const string InvalidInputMessage = "Invalid input! Please try again.";

        private readonly IReader _reader;
        private readonly IParkingConsoleService _parkingService;

        #region Singletone
        private static ParkingConsole _instance;
        private static readonly object _lock = new object();

        private ParkingConsole(IReader reader, IParkingConsoleService parkingService)
        {
            _reader = reader;
            _parkingService = parkingService;
        }
        public static ParkingConsole GetInstance(IReader reader, IParkingConsoleService parkingService)
        {
            if (_instance != null) return _instance;
            lock (_lock)
            {
                _instance ??= new ParkingConsole(reader, parkingService);
            }
            return _instance;
        }
        #endregion

        public void DisplayMainMenu()
        {
            var title = "Please input number of appropriate operation:";
            var content = "1 : Show parking balance"
                             + "\n" + "2 : Show earned sum of current period"
                             + "\n" + "3 : Show count of free spaces"
                             + "\n" + "4 : Show current transactions"
                             + "\n" + "5 : Show transactions from log"
                             + "\n" + "6 : Show list of vehicles at the parking"
                             + "\n" + "7 : Add vehicle to parking"
                             + "\n" + "8 : Take vehicle from parking"
                             + "\n" + "9 : Top up balance of a vehicle"
                           ;
            DisplayMenu(title, content);
            try
            {
                var selectedOperation = _reader.ParseIntNumber(0, 9);
                switch (selectedOperation)
                {
                    case 0:
                        {
                            DisplayMainMenu();
                            return;
                        }
                    case 1:
                        {
                            ShowParkingBalance();
                            break;
                        }
                    case 2:
                        {
                            ShowCurrentSum();
                            break;
                        }
                    case 3:
                        {
                            ShowCountOfFreeSpaces();
                            break;
                        }
                    case 4:
                        {
                            ShowCurrentTransactions();
                            break;
                        }
                    case 5:
                        {
                            ShowTransactionsFromLog();
                            break;
                        }
                    case 6:
                        {
                            ShowVehiclesAtTheParking();
                            break;
                        }
                    case 7:
                        {
                            ShowAddVehicleToParking();
                            break;
                        }
                    case 8:
                        {
                            ShowTakeVehicleFromParking();
                            break;
                        }
                    case 9:
                        {
                            ShowTopUpVehicleBalance();
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                PrintException(ex);
            }
        }


        private void ShowParkingBalance()
        {
            PrintSuccessfulMessage($"Current balance of the parking is: {_parkingService.GetBalance()}");
        }

        private void ShowCurrentSum()
        {
            var sum = _parkingService.GetLastParkingTransactions().Sum(transaction => transaction.Sum);
            PrintSuccessfulMessage($"The amount of money earned for the current period: {sum}");
        }

        private void ShowCountOfFreeSpaces()
        {
            PrintSuccessfulMessage($"Free {_parkingService.GetFreePlaces()} places of {_parkingService.GetCapacity()}");
        }

        private void ShowCurrentTransactions()
        {
            var transactions = _parkingService.GetLastParkingTransactions();
            if (transactions.Length == 0)
            {
                PrintAlertMessage("Now transaction list is empty.");
                return;
            }
            var stringBuilder = new StringBuilder();
            foreach (var transaction in transactions)
            {
                stringBuilder.AppendLine(transaction.ToString());
            }
            PrintSuccessfulMessage($"There is {transactions.Length} transactions:");
            PrintMessage(stringBuilder.ToString(), ConsoleColor.DarkGreen);
        }

        private void ShowTransactionsFromLog()
        {
            try
            {
                var transactionsFromLog = _parkingService.ReadFromLog();
                if (string.IsNullOrWhiteSpace(transactionsFromLog))
                {
                    PrintAlertMessage("Log is empty");
                }
                else
                {
                    PrintSuccessfulMessage("Transactions from log:");
                    PrintMessage(transactionsFromLog, ConsoleColor.DarkGreen);
                }
            }
            catch (InvalidOperationException ex)
            {
                PrintException(ex);
            }
        }

        private void ShowVehiclesAtTheParking()
        {
            var vehicles = _parkingService.GetVehicleList();
            if (!vehicles.Any())
            {
                PrintAlertMessage("Now parking is empty.");
                return;
            }
            var output = vehicles.Aggregate(string.Empty, (current, vehicle) => current + vehicle + (vehicle.Balance >= 0 ? " B+" : " B-") + "\n");
            PrintSuccessfulMessage($"Now we have {vehicles.Count} vehicles at the parking:");
            PrintMessage(output, ConsoleColor.Blue);
        }


        #region Create a vehicle
        private void ShowAddVehicleToParking()
        {
            var vehicleId = GetVehicleId();
            if (vehicleId == null)
            {
                DisplayMainMenu();
                return;
            }

            var vehicleType = GetVehicleType();
            if (vehicleType == 0)
            {
                DisplayMainMenu();
                return;
            }

            var vehicleBalance = DisplayInputNonNegativeNumber();
            if (vehicleBalance == -1)
            {
                DisplayMainMenu();
                return;
            }

            var vehicle = new Vehicle(vehicleId, (VehicleType)vehicleType, vehicleBalance);
            _parkingService.AddVehicle(vehicle);
            PrintSuccessfulMessage($"The vehicle: {vehicle} was added to the parking lot!");
        }

        private string GetVehicleId()
        {
            while (true)
            {
                var vehicleId = string.Empty;
                var content = "1 : Manual input of the registration plate number"
                          + "\n" + "2 : Generate a random registration plate number";
                DisplayMenu("Choose a way to input registration plate number of the vehicle", content);
                try
                {
                    var plateNumberInputWay = _reader.ParseIntNumber(0, 2);
                    switch (plateNumberInputWay)
                    {
                        case 0:
                            {
                                return null;
                            }
                        case 1:
                            {
                                vehicleId = GetManualInputVehicleId();
                                break;
                            }
                        case 2:
                            {
                                vehicleId = Vehicle.GenerateRandomRegistrationPlateNumber();
                                break;
                            }
                    }
                    PrintSuccessfulMessage($"The registration plate number of a vehicle: {vehicleId}");
                    return vehicleId;
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
        }
        private string GetManualInputVehicleId()
        {
            while (true)
            {
                DisplayMenu("Input the registration plate number or 0 to exit", null);
                try
                {
                    var input = _reader.GetNotEmptyString();
                    if (int.TryParse(input, out var code) && code == 0)
                        return null;
                    else if (Regex.IsMatch(input, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"))
                        return input;
                    throw new FormatException(
                        "Please input the registration plate number according to pattern XX-YYYY-XX\nwhere 'X'- char in upper case, 'Y' - a number from 0 to 9");
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
        }

        private int GetVehicleType()
        {
            while (true)
            {
                var vehicleTypes = Enum.GetValues(typeof(VehicleType)).Cast<VehicleType>().ToArray();
                var content = new StringBuilder();
                for (var i = 0; i < vehicleTypes.Length; ++i)
                {
                    content.AppendLine($"{i + 1} : {vehicleTypes[i]}");
                }
                DisplayMenu("Input a number of vehicle type or 0 to exit", content.ToString());
                try
                {
                    return _reader.ParseIntNumber(0, vehicleTypes.Length);
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
        }
        #endregion


        private void ShowTakeVehicleFromParking()
        {
            ShowVehiclesToSelect("Select vehicle to take from parking:");
            try
            {
                var vehicleId = GetVehicleIdByNumberOrId();

                if (vehicleId.Equals("0"))
                    return;

                if (!string.IsNullOrWhiteSpace(vehicleId))
                {
                    _parkingService.RemoveVehicle(vehicleId);
                    PrintSuccessfulMessage($"Vehicle with a plate number {vehicleId} was removed from the parking.\nGood luck!");
                }
                else
                {
                    PrintDanger(InvalidInputMessage);
                    ShowTakeVehicleFromParking();
                }
            }
            catch (Exception ex)
            {
                PrintException(ex);
                ShowTakeVehicleFromParking();
            }
        }

        private void ShowTopUpVehicleBalance()
        {
            ShowVehiclesToSelect("Select vehicle to top up balance:");
            try
            {
                var vehicleId = GetVehicleIdByNumberOrId();

                if (vehicleId.Equals("0"))
                    return;
                if (!string.IsNullOrWhiteSpace(vehicleId))
                {
                    PrintSuccessfulMessage($"Selected vehicle has a registration plate number: {vehicleId}");
                    var sum = DisplayInputNonNegativeNumber();
                    _parkingService.TopUpVehicle(vehicleId, sum);
                    PrintSuccessfulMessage(
                        $"Balance of vehicle with a registration plate number {vehicleId} was top-up by {sum}!");
                }
                else
                {
                    PrintDanger(InvalidInputMessage);
                    ShowTopUpVehicleBalance();
                }
            }
            catch (Exception ex)
            {
                PrintException(ex);
                ShowTopUpVehicleBalance();
            }
        }

        private decimal DisplayInputNonNegativeNumber()
        {
            while (true)
            {
                PrintMessage("Input a non-negative number:", ConsoleColor.Cyan);
                try
                {
                    return _reader.ParseDecimalNumber(0);
                }
                catch (Exception ex)
                {
                    PrintException(ex);
                }
            }
        }
        private void ShowVehiclesToSelect(string title)
        {
            if (!_parkingService.GetVehicleList().Any())//(_parkingService.GetFreePlaces() == _parkingService.GetCapacity())
                throw new InvalidOperationException("Parking is empty");
            var stringBuilder = new StringBuilder();
            var vehicles = _parkingService.GetVehicleList();
            for (var i = 1; i <= vehicles.Count(); ++i)
            {
                stringBuilder.AppendLine($"{i} : {vehicles[i - 1]}");
            }
            DisplayMenu(title, stringBuilder.ToString());
        }
        private string GetVehicleIdByNumberOrId()
        {
            var input = _reader.GetNotEmptyString();
            string vehicleId;

            if (Regex.IsMatch(input, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$"))
            {
                vehicleId = input;
            }
            else
            {
                var number = _reader.ParseIntNumber(0, _parkingService.GetVehicleList().Count, input);
                if (number == 0)
                {
                    return "0";
                }
                vehicleId = _parkingService.GetVehicleList()[number - 1].Id;
            }

            return vehicleId;
        }


        public void DisplayMenu(string title, string content)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("\n" + title);
            Console.ForegroundColor = ConsoleColor.Yellow;
            DisplayBackToMainMenu();
            Console.WriteLine(content);
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void DisplayBackToMainMenu()
        {
            Console.WriteLine("0 : Back to Main menu");
        }

        #region Print messages
        private void PrintSuccessfulMessage(string message)
        {
            PrintMessage(message, ConsoleColor.Green);
        }
        private void PrintAlertMessage(string message)
        {
            PrintMessage(message, ConsoleColor.Yellow);
        }
        private void PrintException(Exception exception)
        {
            PrintDanger(exception.Message);
        }
        private void PrintDanger(string message)
        {
            PrintMessage(message, ConsoleColor.Red);
        }

        private void PrintMessage(string message, ConsoleColor textColor = ConsoleColor.White)
        {
            Console.ForegroundColor = textColor;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.White;
        }
        #endregion
    }
}