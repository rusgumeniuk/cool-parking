﻿using Newtonsoft.Json;

using System;
using System.Globalization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string VehicleId { get; }
        public DateTime TransactionDateTime { get; }
        public decimal Sum { get; }

        [JsonConstructor]
        public TransactionInfo(string vehicleId, DateTime transactionDateTime, decimal sum)
        {
            VehicleId = vehicleId;
            TransactionDateTime = transactionDateTime;
            Sum = sum;
        }

        public override string ToString()
        {
            var enCulture = new CultureInfo("en-US");
            return $"{TransactionDateTime.ToString(enCulture)}: {Sum.ToString(enCulture)} money withdrawn from vehicle with Id='{VehicleId}'.";
        }
    }
}