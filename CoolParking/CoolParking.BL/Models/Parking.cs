﻿using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking _instance;
        private static readonly object _lock = new object();

        public decimal Balance { get; set; }
        public IEnumerable<Vehicle> Vehicles { get; set; }

        private Parking() { }

        public static Parking GetInstance()
        {
            if (_instance != null) return _instance;
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = new Parking { Balance = Settings.ParkingStartBalance, Vehicles = new List<Vehicle>() };
                }
            }
            return _instance;
        }
    }
}