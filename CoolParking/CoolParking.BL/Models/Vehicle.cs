﻿
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = Regex.IsMatch(id, @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$") ? id : throw new ArgumentException();
            VehicleType = vehicleType;
            Balance = balance < 0 ? throw new ArgumentException() : balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var rand = new Random();
            string plateNumber;
            bool isUnique;
            do
            {
                plateNumber =
                    $"{(char)rand.Next(65, 90)}{(char)rand.Next(65, 90)}-{rand.Next(1000, 9999)}-{(char)rand.Next(65, 90)}{(char)rand.Next(65, 90)}";
                isUnique = Parking.GetInstance()
                    .Vehicles
                    .FirstOrDefault(vehicle => vehicle.Id.Equals(plateNumber)) == null;
            } while (!isUnique);

            return plateNumber;
        }

        public override string ToString()
        {
            return $"Id: {Id} | Type: {VehicleType} | Current balance: {Balance}";
        }
    }
}