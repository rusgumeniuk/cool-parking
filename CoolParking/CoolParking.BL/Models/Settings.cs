﻿namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static byte ParkingStartBalance = 0;
        public static byte ParkingCapacity = 10;
        public static uint PayPeriod = 5000; //TODO: or call "N"?
        public static uint LoggingPeriod = 60000;
        public static byte PassengerCarTariff = 2;
        public static byte TruckTariff = 5;
        public static float BusTariff = 3.5f;
        public static byte MotorcycleTariff = 1;
        public static float PenaltyCoefficient = 2.5f;
    }
}