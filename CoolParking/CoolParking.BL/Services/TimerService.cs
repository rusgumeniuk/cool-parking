﻿using CoolParking.BL.Interfaces;

using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer;

        public double Interval
        {
            get => _timer.Interval;
            set => _timer.Interval = value;
        }
        public event ElapsedEventHandler Elapsed
        {
            add => _timer.Elapsed += value;
            remove => _timer.Elapsed -= value;
        }

        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }

        public TimerService()
        {
            _timer = new Timer();
        }
    }
}
