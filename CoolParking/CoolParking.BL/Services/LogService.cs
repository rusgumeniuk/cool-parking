﻿using CoolParking.BL.Interfaces;

using System;
using System.IO;
using System.Text;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string logPath)
        {
            LogPath = logPath;
        }

        public void Write(string logInfo)
        {
            if (string.IsNullOrWhiteSpace(logInfo)) return;
            using (var streamWriter = new StreamWriter(LogPath, true))
            {
                streamWriter.WriteLine(logInfo);
            }
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
                throw new InvalidOperationException("Log file doesn't exist yet. Please wait for any transaction.");

            var stringBuilder = new StringBuilder();
            using (var streamReader = new StreamReader(LogPath))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    stringBuilder.AppendLine(line);
                }
                streamReader.Close();
            }

            return stringBuilder.ToString();
        }
    }
}