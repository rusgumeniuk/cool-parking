﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        private readonly ICollection<TransactionInfo> _transactions;

        private Parking CurrentParking => Parking.GetInstance();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            _transactions = new List<TransactionInfo>();

            _withdrawTimer.Interval = Settings.PayPeriod;
            _logTimer.Interval = Settings.LoggingPeriod;

            _withdrawTimer.Elapsed += WithdrawPayment;
            _logTimer.Elapsed += LogTransactions;
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            var count = _transactions.Count;
            if (count == 0)
            {
                _logService.Write(string.Empty);
            }
            else
            {
                var copyOfTransactions = _transactions.ToList();
                foreach (var transaction in copyOfTransactions)
                {
                    _logService.Write(transaction.ToString());
                }
                _transactions.Clear();//
            }
        }

        private void WithdrawPayment(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in GetVehicles())
            {
                var payment = CalculatePayment(vehicle);
                _transactions.Add(new TransactionInfo(vehicle.Id, DateTime.Now, payment));
                vehicle.Balance -= payment;
                CurrentParking.Balance += payment;
            }
        }

        private decimal CalculatePayment(Vehicle vehicle)
        {
            var payment = 0m;
            switch (vehicle.VehicleType)
            {
                case VehicleType.Bus:
                    {
                        payment = (decimal)Settings.BusTariff;
                        break;
                    }
                case VehicleType.Motorcycle:
                    {
                        payment = Settings.MotorcycleTariff;
                        break;
                    }
                case VehicleType.PassengerCar:
                    {
                        payment = Settings.PassengerCarTariff;
                        break;
                    }
                case VehicleType.Truck:
                    {
                        payment = Settings.TruckTariff;
                        break;
                    }
                default:
                    throw new ArgumentException("Unknown vehicle type");
            }

            if (vehicle.Balance > 0 && vehicle.Balance < payment)
            {
                var debt = (payment - vehicle.Balance) * (decimal)Settings.PenaltyCoefficient;
                payment = debt + vehicle.Balance;
            }
            else if (vehicle.Balance <= 0)
            {
                payment *= (decimal)Settings.PenaltyCoefficient;
            }

            return payment;
        }

        public void Dispose()
        {
            _transactions.Clear();
            CurrentParking.Balance = 0;
            CurrentParking.Vehicles = new List<Vehicle>();
        }

        public decimal GetBalance()
        {
            return CurrentParking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return Settings.ParkingCapacity - CurrentParking.Vehicles.Count();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return CurrentParking.Vehicles.ToList().AsReadOnly();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null)
                throw new ArgumentNullException();
            if (CurrentParking.Vehicles.FirstOrDefault(veh => veh.Id.Equals(vehicle.Id)) != null)
                throw new ArgumentException($"Parking already consists vehicle with id {vehicle.Id}");
            if (GetFreePlaces() < 1)
                throw new InvalidOperationException();

            CurrentParking.Vehicles = CurrentParking.Vehicles.Append(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = GetVehicle(vehicleId);

            if (vehicle.Balance < 0)
            {
                throw new InvalidOperationException(
                    $"Vehicle with id {vehicleId} has a negative balance and can't be removed");
            }

            CurrentParking.Vehicles = CurrentParking.Vehicles.Where(veh => !veh.Id.Equals(vehicleId));
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= decimal.Zero)
                throw new ArgumentException($"Sum should be more than 0. Value {sum} is incorrect.");
            var vehicle = GetVehicle(vehicleId);
            _transactions.Add(new TransactionInfo(vehicleId, DateTime.Now, sum));
            vehicle.Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return _transactions.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        /// <summary>
        /// Returns a vehicle with a specific plate number.
        /// </summary>
        /// <param name="vehicleId">Registration plate number of a vehicle</param>
        /// <exception cref="ArgumentException">Throws exception when parking doesn't contain vehicle with that plate number</exception>
        /// <exception cref="InvalidOperationException">Throw exception if parking has 2+ vehicles with same plate number</exception>
        /// <returns></returns>
        private Vehicle GetVehicle(string vehicleId)
        {
            var vehicle = GetVehicleOrDefault(vehicleId);
            return vehicle ?? throw new ArgumentException($"There is no vehicle with that id: {vehicleId}");
        }
        /// <summary>
        /// Returns a vehicle with a specific plate number or default if nothing found. Throws exception if found 2+ vehicles with the same plate number.
        /// </summary>
        /// <param name="vehicleId">Registration plate number of a vehicle</param>
        /// <exception cref="InvalidOperationException">Throws exception if parking has 2+ vehicles with same plate number</exception>
        /// <returns></returns>
        private Vehicle GetVehicleOrDefault(string vehicleId)
        {
            if (string.IsNullOrWhiteSpace(vehicleId))
                return null;
            return CurrentParking.Vehicles.SingleOrDefault(vehicle => vehicle.Id.Equals(vehicleId));
        }
    }
}